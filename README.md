# CppDatabaseInteraction
CppDatabaseInteraction is a tool to converted exported contacts from tutanota into a valid vCard file for migration of address books to other organizer software / Email clients like Thunderbird.

## Installation
1. Download the installer CppDatabaseInteraction-x.y.z-win64.exe from the release or build and package from the source as described in section "Distribute CppDatabaseInteraction".
1. Run the installer CppDatabaseInteraction-x.y.z-win64.exe and follow the instructions on screen.

## Usage
1. In tutanota: Manually format the addresses so that each address has one of the following formats: 
Either
```
Street
Locality
Region
Postal Code
Country
```
or
```
Street
Locality
Postal Code
Country
```

or
```
Street
Locality
Region
Postal Code
```
or
```
Street
Locality
Postal Code
```
Use dummy values (e.g. `0000` for Postal code, `NA` for other address components) as placeholders for missing values of partially existing address information.

2. In tutanota: Click on '...' next to 'All Contacts'
3. In tutanota: Click on 'export vCard'. Enter a file name (e.g. tutanotacontacts) and select the target directory. Click on 'Save'
4. Navigate to the Powershell_ISE.exe and double-click the executable to run it.
5. In the command prompt, type the following and confirm by pressing the enter-key:

- Method A:
```
.CppDatabaseInteraction --infile "<Path\to\file\tutanotacontacts.vcf>" --outfile "<Path\to\output\file\vCardv3.vcf>"
```

- Method B:
```
<Path\to\parentdirectory\of\CppDatabaseInteraction.exe> --infile "<Path\to\file\tutanotacontacts.vcf>" --outfile "<Path\to\output\file\vCardv3.vcf>"
```

- Method C:
```
cd "<Path\to\directory\of\tutanotacontacts.exe\>"
.CppDatabaseInteraction --infile "<tutanotacontacts.vcf>" --outfile "<Path\to\output\file\vCardv3.vcf>"
```

## Support
https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/issues

## Contributing
Merge requests (pull requests) targeting the main branch are welcome. For details, see file [CONTRIBUTING.md](./CONRTIBUTING.md).

## Developing CppDatabaseInteraction
1. Install MSYS2 and update its mingw, including gcc, make and cmake and add edit the System environment variables to add the path to these tools to the System environment variable PATH.
1. Install Microsoft Visual Studio code (VScode)
1. In VScode, install the extension "C/C++ Extension" by Microsoft
1. In VScode, install the extension "CMake tools" by Microsoft
1. In VScode, go to File -\> Preferences -\> Settings. Set the path to those tools. For CMake: Go to submenu Extensions -\> CMake Tools, section "Cmake: Configure Settings" -\> "Edit in settings.json". In that settings.jason, [add the following](https://github.com/microsoft/vscode-cmake-tools/issues/880):
```
    "cmake.cmakePath": "C:\\msys64\\mingw64\\bin\\cmake.exe",
    "cmake.mingwSearchDirs": [
      "C:\\msys64\\mingw64\\bin"
   ],
   "cmake.generator": "MinGW Makefiles"
```
1. Get the source code for CppDatabaseInteraction
by forking CppDatabaseInteraction and cloning the fork using git. If you want to download only
the current files of the selected branch, click the control panel "Download"
(next to the control panel "clone").

### Build CppDatabaseInteraction

In VScode, do the following:
1. Go to View -\> Command Palette. Execute the following commands:
```
CMake: Scan kits
CMake: Select kit
CMake: Configure
CMake: Select Variant -> Debug
CMake: Configure
CMake: Build
CMake: Debug
```
1. Go to "Run and Debug". Left-click the dropdown menu next to "Start Debugging", select "Add config (projectname)". Select "C/C++: (Windows) Launch".
1. open the newly created file launch.json and edit it, if required
1. Go to "Run and Debug" -\> "Start Debugging"

See https://www.youtube.com/watch?v=kEGQKzhciKc&list=PLalVdRk2RC6o5GHu618ARWh0VO0bFlif4&index=2

### Distribute CppDatabaseInteraction
1. Install NSIS
1. Update all version numbers and push the changes
1. Go to View -\> Command Palette. Execute the following commands:
```
CMake: Select Variant -> Release
CMake: Configure
CMake: Build
```
1. In the terminal which runs the PowerShell, change directory to the subdirectory "build" of the CppDatabaseInteraction project folder and run cpack:
```
cd build
cpack
``` 
1. Create a tag for the last commit (e.g. tag: 1.0.1, message: "tagged release of CppDatabaseInteractioner v1.0.1" and push the changes
Download https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/-/archive/main/CppDatabaseInteraction-main.tar.gz and extract the archive.
1. In VScode, Go to View -\> Command Palette. Execute the following commands:
```
CMake: Select Variant -> Release
CMake: Configure
CMake: Build
``` 
1. Log in to gitlab.com
and go to [gitlab.com/DataAnalysisByDerAndere/ReproducibleWorkflowsByDerAndere/CppDatabaseInteraction -\> Deployments -\> Releases](https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/-/releases)

Left-click "New Release". Release name: v1.0.1, release notes: tagged release of CppDatabaseInteraction 1.0.1 and attach the CppDatabaseInteraction installer (build/_CPack_Packages/win64/NSIS/CppDatabaseInteraction-x.y.z-win64/CppDatabaseInteraction-x.y.z-win64.exe).
Left-click "OK"


## Credits
CppDatabaseInteraction (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction)
Copyright 2018 - 2022 The CppDatabaseInteraction Authors (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/blob/main/AUTHORS)
Copyright 2022 [DerAndere](https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/issues) ([Donate](https://www.paypal.com/donate/?hosted_button_id=TNGG65GVA9UHE))


## License information
```
CppDatabaseInteraction (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction)
Copyright 2015 - 2022 The CppDatabaseInteraction Authors (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/blob/main/AUTHORS)
Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/issues)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2.0, as
published by the Free Software Foundation.

This program is also distributed with certain software (including
but not limited to OpenSSL) that is licensed under separate terms,
as designated in a particular file or component or in included license
documentation.  The authors of MySQL hereby grant you an
additional permission to link the program and your derivative works
with the separately licensed software that they have included with
MySQL.

Without limiting anything contained in the foregoing, this file,
which is part of MySQL Connector/C++, is also subject to the
Universal FOSS Exception, version 1.0, a copy of which can be found at
http://oss.oracle.com/licenses/universal-foss-exception.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License, version 2.0, for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA


This software includes software developed by DerAndere.

Contributions by DerAndere
Copyright 2022 DerAndere (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/issues).

This software includes software developed by The CppDatabaseInteraction Authors.

Contributions by The CppDatabaseInteraction Authors
Copyright 2015 - 2022 The CppDatabaseInteraction Authors (https://gitlab.com/DataAnalysisByDerAndere/DatabaseInteractionByDerAndere/CppDatabaseInteraction/issues).

This software includes software developed by Oracle and/or it's affiliates.

Contributions by Oracle and/or it's affiliates
Copyright (c) 2015, 2018, Oracle and/or its affiliates. All rights reserved.
```
